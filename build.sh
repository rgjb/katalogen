#!/bin/bash

rm -r build/

mkdir -p build
mkdir -p build/api
mkdir -p build/lib

BASEDIR=$(cd `dirname $0` && pwd)
DB=$BASEDIR/db
TEMPLATES=$BASEDIR/templates
BUILD=$BASEDIR/build
SCRIPT=$BASEDIR/script

recsel -t Programvara -j User -p Id,Name,Url,Description,Keyword,Publiccode,User_Name,User_Id $DB/programvaror.rec $DB/organisationer.rec \
| recfmt -f $TEMPLATES/rec2json.template \
| jq -sR '. | gsub("[\\n\\t]"; " ")' \
| jq -r '.' \
| jq -s '{katalog:.}' \
| jq '{ "katalog": ( .katalog | group_by(.id) | map({"id": .[0].id, "name": .[0].name, "description":  .[0].description,  "url":  .[0].url, "keyword": .[0].keyword, "publiccode": .[0].publiccode, "user": map(.user)}) ) } | del(..|select(. == ""))' \
| jq '{ "katalog": ( .katalog | sort_by(.name) )}' > $BUILD/api/katalog.json

# Paging, dela in arrayen i grupper om 10 för en enklare presentation. Ej en del av API:et / datasetet.
# cat $BUILD/api/katalog.json | jq '.katalog | _nwise(10) | {page:.}' | jq -s '{katalog_pag:.}' > $BUILD/api/katalog_pag.json
cat $BUILD/api/katalog.json | jq '[.katalog | _nwise(10)] | to_entries | .[] | {page:.value,id:.key}' | jq -s '{katalog_pag:.}' > $BUILD/api/katalog_pag.json

cat $BUILD/api/katalog_pag.json | node_modules/mustache/bin/mustache - $TEMPLATES/programvaror.mustache > $BUILD/index.html

#
# Skapa ett index med lunrjs
#
cat $BUILD/api/katalog.json | jq -c '.katalog' | node $SCRIPT/index.js > $BUILD/index.json

cp lib/lunr.js $BUILD/lib
cp lib/lunr.min.js $BUILD/lib
cp lib/script.js $BUILD/lib
cp lib/style.css $BUILD/lib

rm -r public/

mkdir -p public
#mkdir -p public/api

find $BUILD -type f -exec brotli \{\} \;

cp -R build/. public/

#cp $BUILD/index.html public
#cp $BUILD/index.json public
#cp $BUILD/katalog.json public/api
