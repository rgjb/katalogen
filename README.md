# Readme

[Offentligkod.se](https://offentligkod.se) är en produkt av nätverket nosad.se. Eftersom intresset har varit stor för en listning av öppna programvaror skapades en egen sajt.

![Personer](https://img.shields.io/badge/dynamic/json?color=brightgreen&label=Antal%20personer%20som%20bidrar&query=%24.length&url=https%3A%2F%2Fgitlab.com%2Fapi%2Fv4%2Fprojects%2F26530770%2Frepository%2Fcontributors&style=for-the-badge&logo=gitlab)

## Tillägg och uppdateringar av listan

Se [manual för bidrag](CONTRIBUTING.md)

## Arkitektur för webbsidan

Sajten utgörs av tre huvudfiler.

1. index.html - Motsvarar offentligkod.se.
2. index.json - Ett index som aavänds för sökning.
3. katalog.json - Ett API i maskinläsbart format.

Sidan uppdateras automatiskt om en förändringar görs i repot.

### API

https://offentligkod.se/api

Webbservern servar katalog.json under /api. Se [konfiguration](_redirects).

### Byggscript

Vid ändringar i filen [programvaror.rec](db/programvaror.rec) etc skapas en ny version av sajten. [CI/CD-bygget kör på gitlab](.gitlab-ci.yml) och startar ett [script](build.sh) som genererar de tre statiska filerna (index.html, index.json och katalog.json). Html-filen byggs utifrån en [template](templates/programvaror.mustache).
